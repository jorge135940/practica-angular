import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  frase: any = {
    mensaje: 'La sabiduría viene de la experiencia. La experiencia es, a menudo, el resultado de la falta de sabiduría',
    autor: 'Terry Pratchett',
  };

  mostrar:boolean = false; 
  valorNumerico: number = 1;
  personas: string[] = ['Pedro', 'Carlos', 'María'];

  constructor() { }

  ngOnInit(): void {
  }

}
